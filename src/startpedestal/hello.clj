(ns startpedestal.hello
  (:require [io.pedestal.http :as http]
            [io.pedestal.http.route :as route]
            [hiccup2.core :as h]
            [startpedestal.render :as r]))

(defn ok [body]
  {:status 200
   :headers {"Content-Type" "text/html"
             "Content-Security-Policy" "script-src 'self' 'unsafe-inline' 'unsafe-eval' *"}
             :body body})

(defn respond-hello [request]
  (let [name (get-in request [:query-params :name] "World")]
    (ok (r/template (str (h/html [:h1 { :class "title is-1" } "Hello, " name "!"]) )))))

(defn click-me [request]
  (ok
   (r/template
   (h/html [:div
            [:button {:hx-post "/clicked" :hx-swap "outerHTML"} "Click Me!"]]))))

(defn clicked [request]
  (ok 
   (str (h/html [:div "You clicked me!"]))))

(def routes
  (route/expand-routes
   #{["/greet" :get respond-hello :route-name :greet]
     ["/click-me" :get click-me :route-name :click-me]
     ["/clicked" :post clicked :route-name :clicked]}))

(def service-map
  {::http/routes routes
   ::http/type :jetty
   ::http/port 8890})

(defn start []
  (http/start (http/create-server service-map)))

(defonce server (atom nil))

(defn start-dev []
  (reset! server
          (http/start (http/create-server
                       (assoc service-map
                              ::http/join? false)))))

(defn stop-dev []
  (http/stop @server))

(defn restart []
  (stop-dev)
  (start-dev))
