(ns startpedestal.render
  (:require [hiccup2.core :as h]))

(defn css [location]
  (h/html [:link {:rel "stylesheet" :href location}]))

(defn js [location]
  (h/html [:script {:type "text/javascript" :src location}]))

(defn favicon [location]
  (h/html [:link {:rel "shortcut icon" :href location}]))

(defn title [content]
  (h/html [:title content]))

(def doc-html "<!DOCTYPE html>")

(defn head []
   (h/html [:head (title "My pedestal app")
            (favicon "https://yu7.in/clojure-diary-logo")
            (css "https://cdn.jsdelivr.net/npm/bulma@0.9.4/css/bulma.min.css")
            (js "https://unpkg.com/htmx.org@1.9.4")]))

(defn template [html-body]
  (str (h/html [:html
                (head)
                [:body (h/raw html-body)]])))
